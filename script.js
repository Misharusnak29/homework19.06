/* Теоретичні питання 
1.Метод forEach використовується для виконання певної функції для кожного елемента масиву. Ця функція приймає до трьох аргументів: поточний елемент, індекс цього елемента та сам масив. Основна особливість методу forEach полягає в тому, що він не повертає новий масив і не змінює існуючий, а просто виконує функцію для кожного елемента масиву.

2.Методи, що мутують існуючий масив:
- `push()`
- `pop()`
- `shift()`
- `unshift()`
- `splice()`
- `sort()`
- `reverse()`

Методи, що повертають новий масив:
- `map()`
- `filter()`
- `slice()`
- `concat()`
- `flat()`
- `reduce()`

3.Для перевірки чи є змінна масивом, використовується метод `Array.isArray()`:

const data = [1, 2, 3];
console.log(Array.isArray(data));

4.- Метод `map()` варто використовувати, коли потрібно створити новий масив, що містить результати виконання функції для кожного елемента вихідного масиву.
- Метод `forEach()` варто використовувати, коли потрібно виконати якусь операцію для кожного елемента масиву, але не потрібно створювати новий масив.

*/

// Практичні завдання
// 1.

// const strings = ["travel", "hello", "eat", "ski", "lift"];
// const count = strings.filter(str => str.length > 3).length;
// alert(count);

// 2.

// const people = [
//     { name: "Іван", age: 25, sex: "чоловіча" },
//     { name: "Олена", age: 30, sex: "жіноча" },
//     { name: "Петро", age: 35, sex: "чоловіча" },
//     { name: "Марія", age: 28, sex: "жіноча" }
// ];
// const men = people.filter(person => person.sex === "чоловіча");
// console.log(men);

// 3.

// function filterBy(arr, type) {
//     return arr.filter(item => typeof item !== type);
// }

// const mixedArray = ['hello', 'world', 23, '23', null];
// const filteredArray = filterBy(mixedArray, 'string');
// console.log(filteredArray);
